alembic==1.8.1

aniso8601==8.0.0

attrs==20.3.0

authlib==0.15.3
bcrypt==3.1.7

boto3==1.13.9

botocore==1.16.26

certifi==2020.11.8

cffi==1.15.0

chardet==3.0.4

click==7.1.2

cmarkgfm==0.8.0

dataset==1.3.1

docutils==0.15.2

flask-caching==2.0.1
flask-marshmallow==0.10.1

flask-migrate==2.5.3

flask-restx==0.5.1

flask-script==2.0.6

flask-sqlalchemy==2.4.3

flask==1.1.2

gevent==21.12.0

greenlet==1.1.1

gunicorn==20.0.4

idna==2.10

importlib-metadata==4.12.0
itsdangerous==1.1.0

jinja2==2.11.3

jmespath==0.10.0

jsonschema==3.2.0

kubernetes==24.2.0
mako==1.1.3

markupsafe==1.1.1

marshmallow-sqlalchemy==0.17.0


marshmallow==2.20.2
maxminddb==1.5.4

passlib==1.7.2

pybluemonday==0.0.9

pycparser==2.20

pydantic==1.6.2

pymysql==0.9.3

pyrsistent==0.17.3

python-dateutil==2.8.1

python-dotenv==0.13.0

python-editor==1.0.4

python-geoacumen-city==2022.2.15

pytz==2020.4

redis==3.5.2

requests==2.23.0

s3transfer==0.3.3

six==1.15.0

sqlalchemy-utils==0.36.6

sqlalchemy==1.3.17
tenacity==6.2.0

typing-extensions==4.3.0

urllib3==1.25.11
werkzeug==1.0.1

wtforms==2.3.3

zipp==3.8.0
zope-event==4.5.0

zope.event==4.5.0
zope.interface==5.4.0