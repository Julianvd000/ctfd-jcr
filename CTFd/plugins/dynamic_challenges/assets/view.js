CTFd._internal.challenge.data=void 0,CTFd._internal.challenge.renderer=CTFd.lib.markdown(),CTFd._internal.challenge.preRender=function(){},CTFd._internal.challenge.postRender=function(){},CTFd._internal.challenge.render=function(e){return CTFd._internal.challenge.renderer.render(e)},CTFd._internal.challenge.submit=function(e){var n={},l={challenge_id:parseInt(CTFd.lib.$("#challenge-id").val()),submission:CTFd.lib.$("#challenge-input").val()};return e&&(n.preview=!0),CTFd.api.post_challenge_attempt(n,l).then(function(e){return 429===e.status?e:(e.status,e)})};

function start_challenge(challenge_id, user_id, nonce) {

    $('#deployment').html('<div class="text-center"><i class="fas fa-circle-notch fa-spin fa-1x"></i></div>');

    json_body = { "challenge_id": challenge_id, "user_id": user_id };
    headers = { "CSRF-Token": nonce, 'Content-Type': 'application/json' };

    $.ajax({
        type: 'POST',
        url: '/dynamic_challenges/start',
        headers: headers,
        dataType: 'json',
        data: JSON.stringify(json_body),
        success: function (result) {
            $("#deployment").html('<div><button onclick="stop_challenge(' + challenge_id + ',' + user_id + ',' + '\'' + nonce + '\'' +')" class="btn btn-outline-secondary"><i class="fas fa-play"></i> Stop Challenge</button><div><div class="table-responsive" id="deployinfo"></div>');
            return;
        },
        error: function (result) {
            $('#deployment').html('<div class="text-center">Cannot start challenge!</div>');
            return;
        }
    });
};

function stop_challenge(challenge_id, user_id, nonce) {

    if (typeof challenge_id === "number") {
        challenge_id = String(challenge_id);
    }
    if (typeof user_id === "number") {
        user_id = String(user_id);
    }

    json_body = { "challenge_id": challenge_id, "user_id": user_id };
    headers = { "CSRF-Token": nonce, 'Content-Type': 'application/json' };

    $.ajax({
        type: 'POST',
        url: '/dynamic_challenges/stop',
        headers: headers,
        dataType: 'json',
        data: JSON.stringify(json_body),
        success: function (result) {
            $('#deployment').html('<div class="text-center">Challenge stopped!</div>');
            return;
        },
        error: function (result) {
            $('#deployment').html('<div class="text-center">Cannot stop challenge!</div>');
            return;
        }
    });
};