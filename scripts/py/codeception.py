import flag
import base64

#Create flag
ascii = flag.create_flag()
print("ascii: " + ascii)

#Encode with hex
hex = ascii.encode("utf-8").hex()
print("hex: " + hex)

#Encode with base64
base64 = base64.b64encode(hex.encode('ascii')).decode('ascii')
print("base64: " + base64)

#Encode with binary
binary = " ".join(f"{ord(i):08b}" for i in base64)
print("binary: " + binary)